<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>-----------------------------------------------------------------------------------------------</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th style="text-align:left">Model</th>
                        <th style="text-align:left">Handy</th>
                        <th style="text-align:left">Origin</th>
                        <th style="text-align:left">Range</th>
                        <th style="text-align:left">Sight Range</th>
                        <th style="text-align:left">Has Clip</th>
                        <th style="text-align:left">Has Optical Sight</th>
                        <th style="text-align:left">Material</th>
                    </tr>
                    <xsl:for-each select="guns/gun">
                        <tr>
                            <td><xsl:value-of select="model"/></td>
                            <td><xsl:value-of select="handy"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="range"/></td>
                            <td><xsl:value-of select="sightRange"/></td>
                            <td><xsl:value-of select="hasClip"/></td>
                            <td><xsl:value-of select="hasOpticalSight"/></td>
                            <td><xsl:value-of select="material"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

