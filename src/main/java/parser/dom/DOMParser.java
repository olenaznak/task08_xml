package parser.dom;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import model.Gun;
import model.TTC;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class DOMParser {
    List<Gun> gunsList = new ArrayList<>();
    TTC ttc = new TTC();

    public List<Gun> parseGuns(File xml) {
        DOMDocCreator docCreator = new DOMDocCreator(xml);
        Document doc = docCreator.getDocument();
        try {
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("gun");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Gun gun = new Gun();
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    gun.setId(Integer.parseInt(eElement.getAttribute("id")));
                    gun.setModel(eElement.getElementsByTagName("model").item(0).getTextContent());
                    gun.setHandy(Integer.parseInt(eElement.getElementsByTagName("handy").item(0).getTextContent()));
                    gun.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                    gun.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                    ttc = getTTC(eElement.getElementsByTagName("ttc"));
                    gun.setMaterial(eElement.getElementsByTagName("material").item(0).getTextContent());

                    gun.setTtc(ttc);
                }
                gunsList.add(gun);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gunsList;
    }

    private TTC getTTC (NodeList nodes) {
        TTC ttc = new TTC();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            ttc.setRange(element.getElementsByTagName("range").item(0).getTextContent());
            ttc.setSightingRange(Integer.parseInt(element.getElementsByTagName("sightingRange").item(0).getTextContent()));
            ttc.setHasClip(Boolean.parseBoolean(element.getElementsByTagName("hasClip").item(0).getTextContent()));
            ttc.setHasOpticalSight(Boolean.parseBoolean(element.getElementsByTagName("hasOpticalSight").item(0).getTextContent()));
        }
        return ttc;
    }
}
