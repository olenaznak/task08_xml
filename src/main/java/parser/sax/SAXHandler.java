package parser.sax;

import model.Gun;
import model.TTC;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Gun> gunList = new ArrayList<>();
    private Gun gun = null;
    private TTC ttc = null;

    private boolean bModel = false;
    private boolean bHandy = false;
    private boolean bOrigin = false;
    private boolean bTTC = false;
    private boolean bRange = false;
    private boolean bSightRange = false;
    private boolean bHasClip = false;
    private boolean bHasOpticalSight = false;
    private boolean bMaterial = false;

    public List<Gun> getGunList(){
        return this.gunList;
    }

    public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("gun")){
            String id = attributes.getValue("id");
            gun = new Gun();
            gun.setId(Integer.parseInt(id));
        }
        else if (qName.equalsIgnoreCase("model")){bModel = true;}
        else if (qName.equalsIgnoreCase("handy")){bHandy = true;}
        else if (qName.equalsIgnoreCase("origin")){bOrigin = true;}
        else if (qName.equalsIgnoreCase("ttc")){bTTC = true;}
        else if (qName.equalsIgnoreCase("range")){bRange = true;}
        else if (qName.equalsIgnoreCase("sightRange")){bSightRange = true;}
        else if (qName.equalsIgnoreCase("hasClip")){bHasClip = true;}
        else if (qName.equalsIgnoreCase("hasOpticalSight")){bHasOpticalSight = true;}
        else if (qName.equalsIgnoreCase("material")){bMaterial = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("gun")){
            gunList.add(gun);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bModel){
            gun.setModel(new String(ch, start, length));
            bModel = false;
        }
        else if (bHandy){
            gun.setHandy(Integer.parseInt(new String(ch,start,length)));
            bHandy = false;
        }
        else if (bOrigin){
            gun.setOrigin(new String(ch, start, length));
            bOrigin = false;
        }
        else if (bTTC){
            ttc = new TTC();
            bTTC = false;
        }
        else if(bRange){
            ttc.setRange(new String(ch,start,length));
            bRange = false;
        }
        else if (bSightRange){
            ttc.setSightingRange(Integer.parseInt(new String(ch,start,length)));
            bSightRange = false;
        }
        else if (bHasClip){
            ttc.setHasClip(Boolean.parseBoolean(new String(ch,start,length)));
            bHasClip = false;
        }
        else if (bHasOpticalSight){
            ttc.setHasOpticalSight(Boolean.parseBoolean(new String(ch,start,length)));
            bHasOpticalSight = false;
        }
        else if (bMaterial){
            gun.setMaterial(new String(ch,start,length));
            gun.setTtc(ttc);
            bMaterial = false;
        }
    }
}

