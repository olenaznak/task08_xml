package parser.stax;

import model.Gun;
import model.TTC;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXParser {
    List<Gun> gunsList = new ArrayList<>();
    TTC ttc = new TTC();
    Gun gun;

    public List<Gun> parseGuns(File xml){
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "gun":
                            gun = new Gun();

                            Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                            if (idAttr != null) {
                                gun.setId(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "model":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setModel(xmlEvent.asCharacters().getData());
                            break;
                        case "handy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setHandy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "ttc":
                            xmlEvent = xmlEventReader.nextEvent();
                            ttc = new TTC();
                            break;
                        case "range":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setRange(xmlEvent.asCharacters().getData());
                            break;
                        case "sightingRange":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setSightingRange(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "hasClip":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setHasClip(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "hasOpticalSight":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ttc != null;
                            ttc.setHasOpticalSight(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "material":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setMaterial(xmlEvent.asCharacters().getData());
                            assert ttc != null;
                            gun.setTtc(ttc);
                            break;
                    }
                }
                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("gun")){
                        gunsList.add(gun);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return gunsList;
    }
}
