import model.Gun;
import parser.dom.DOMParser;
import parser.sax.MySAXParser;
import parser.stax.StAXParser;

import java.io.File;
import java.util.List;

public class Parser {
    public static void main (String[] args) {
      File xml = new File("src\\main\\resources\\gunXML.xml");
        File xsd = new File("src\\main\\resources\\gunXSD.xsd");

        System.out.println("SAX parser");
        List<Gun> gunsSAX = MySAXParser.parseGuns(xml, xsd);
        gunsSAX.stream().map(Gun::toString).forEach(System.out::println);

        System.out.println("\nDOM parser");
        DOMParser domParser = new DOMParser();
        List<Gun> gunsDOM = domParser.parseGuns(xml);
        gunsDOM.stream().map(Gun::toString).forEach(System.out::println);

        System.out.println("\nStAX parser");
        StAXParser staxParser = new StAXParser();
        List<Gun> gunsStAX = staxParser.parseGuns(xml);
        gunsStAX.stream().map(Gun::toString).forEach(System.out::println);
    }

}
